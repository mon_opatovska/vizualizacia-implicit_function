/********************************************************************************
** Form generated from reading UI file 'implicitdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_IMPLICITDIALOG_H
#define UI_IMPLICITDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_implicitdialog
{
public:
    QVBoxLayout *verticalLayout;
    QFormLayout *formLayout;
    QDoubleSpinBox *spinXstart;
    QLabel *label;
    QLabel *label_2;
    QDoubleSpinBox *spinXend;
    QLabel *label_4;
    QLabel *label_3;
    QDoubleSpinBox *spinYstart;
    QDoubleSpinBox *spinYend;
    QLabel *label_5;
    QLabel *label_6;
    QDoubleSpinBox *spinZstart;
    QDoubleSpinBox *spinZend;
    QLabel *label_7;
    QHBoxLayout *horizontalLayout;
    QLineEdit *implLine;
    QLabel *label_8;
    QSpacerItem *verticalSpacer;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer;
    QPushButton *pushButton;
    QPushButton *pushButton_2;

    void setupUi(QDialog *implicitdialog)
    {
        if (implicitdialog->objectName().isEmpty())
            implicitdialog->setObjectName(QStringLiteral("implicitdialog"));
        implicitdialog->resize(474, 283);
        verticalLayout = new QVBoxLayout(implicitdialog);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        formLayout = new QFormLayout();
        formLayout->setSpacing(6);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        spinXstart = new QDoubleSpinBox(implicitdialog);
        spinXstart->setObjectName(QStringLiteral("spinXstart"));
        spinXstart->setDecimals(6);
        spinXstart->setMinimum(-999999);
        spinXstart->setMaximum(1e+6);

        formLayout->setWidget(0, QFormLayout::FieldRole, spinXstart);

        label = new QLabel(implicitdialog);
        label->setObjectName(QStringLiteral("label"));

        formLayout->setWidget(0, QFormLayout::LabelRole, label);

        label_2 = new QLabel(implicitdialog);
        label_2->setObjectName(QStringLiteral("label_2"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label_2);

        spinXend = new QDoubleSpinBox(implicitdialog);
        spinXend->setObjectName(QStringLiteral("spinXend"));
        spinXend->setDecimals(6);
        spinXend->setMinimum(-999999);
        spinXend->setMaximum(1e+6);
        spinXend->setValue(10);

        formLayout->setWidget(1, QFormLayout::FieldRole, spinXend);

        label_4 = new QLabel(implicitdialog);
        label_4->setObjectName(QStringLiteral("label_4"));

        formLayout->setWidget(3, QFormLayout::LabelRole, label_4);

        label_3 = new QLabel(implicitdialog);
        label_3->setObjectName(QStringLiteral("label_3"));

        formLayout->setWidget(2, QFormLayout::LabelRole, label_3);

        spinYstart = new QDoubleSpinBox(implicitdialog);
        spinYstart->setObjectName(QStringLiteral("spinYstart"));
        spinYstart->setDecimals(6);
        spinYstart->setMinimum(-999999);
        spinYstart->setMaximum(1e+6);

        formLayout->setWidget(2, QFormLayout::FieldRole, spinYstart);

        spinYend = new QDoubleSpinBox(implicitdialog);
        spinYend->setObjectName(QStringLiteral("spinYend"));
        spinYend->setDecimals(6);
        spinYend->setMinimum(-999999);
        spinYend->setMaximum(1e+6);
        spinYend->setValue(10);

        formLayout->setWidget(3, QFormLayout::FieldRole, spinYend);

        label_5 = new QLabel(implicitdialog);
        label_5->setObjectName(QStringLiteral("label_5"));

        formLayout->setWidget(4, QFormLayout::LabelRole, label_5);

        label_6 = new QLabel(implicitdialog);
        label_6->setObjectName(QStringLiteral("label_6"));

        formLayout->setWidget(5, QFormLayout::LabelRole, label_6);

        spinZstart = new QDoubleSpinBox(implicitdialog);
        spinZstart->setObjectName(QStringLiteral("spinZstart"));
        spinZstart->setDecimals(6);
        spinZstart->setMinimum(-999999);
        spinZstart->setMaximum(1e+6);

        formLayout->setWidget(4, QFormLayout::FieldRole, spinZstart);

        spinZend = new QDoubleSpinBox(implicitdialog);
        spinZend->setObjectName(QStringLiteral("spinZend"));
        spinZend->setDecimals(6);
        spinZend->setMinimum(-999999);
        spinZend->setMaximum(1e+6);
        spinZend->setValue(10);

        formLayout->setWidget(5, QFormLayout::FieldRole, spinZend);

        label_7 = new QLabel(implicitdialog);
        label_7->setObjectName(QStringLiteral("label_7"));

        formLayout->setWidget(6, QFormLayout::LabelRole, label_7);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        implLine = new QLineEdit(implicitdialog);
        implLine->setObjectName(QStringLiteral("implLine"));

        horizontalLayout->addWidget(implLine);

        label_8 = new QLabel(implicitdialog);
        label_8->setObjectName(QStringLiteral("label_8"));

        horizontalLayout->addWidget(label_8);


        formLayout->setLayout(6, QFormLayout::FieldRole, horizontalLayout);


        verticalLayout->addLayout(formLayout);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);

        pushButton = new QPushButton(implicitdialog);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        horizontalLayout_2->addWidget(pushButton);

        pushButton_2 = new QPushButton(implicitdialog);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));

        horizontalLayout_2->addWidget(pushButton_2);


        verticalLayout->addLayout(horizontalLayout_2);


        retranslateUi(implicitdialog);
        QObject::connect(pushButton, SIGNAL(clicked()), implicitdialog, SLOT(accept()));
        QObject::connect(pushButton_2, SIGNAL(clicked()), implicitdialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(implicitdialog);
    } // setupUi

    void retranslateUi(QDialog *implicitdialog)
    {
        implicitdialog->setWindowTitle(QApplication::translate("implicitdialog", "implicitdialog", 0));
        label->setText(QApplication::translate("implicitdialog", "x start", 0));
        label_2->setText(QApplication::translate("implicitdialog", "x end", 0));
        label_4->setText(QApplication::translate("implicitdialog", "y end", 0));
        label_3->setText(QApplication::translate("implicitdialog", "y start", 0));
        label_5->setText(QApplication::translate("implicitdialog", "z start", 0));
        label_6->setText(QApplication::translate("implicitdialog", "z end", 0));
        label_7->setText(QApplication::translate("implicitdialog", "Implicit function", 0));
        implLine->setText(QApplication::translate("implicitdialog", "x", 0));
        label_8->setText(QApplication::translate("implicitdialog", "=0", 0));
        pushButton->setText(QApplication::translate("implicitdialog", "Ok", 0));
        pushButton_2->setText(QApplication::translate("implicitdialog", "Cancel", 0));
    } // retranslateUi

};

namespace Ui {
    class implicitdialog: public Ui_implicitdialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_IMPLICITDIALOG_H
