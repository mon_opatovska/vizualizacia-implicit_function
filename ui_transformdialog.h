/********************************************************************************
** Form generated from reading UI file 'transformdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TRANSFORMDIALOG_H
#define UI_TRANSFORMDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_TransformDialog
{
public:
    QVBoxLayout *verticalLayout;
    QFormLayout *formLayout;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;
    QLineEdit *xLine;
    QLineEdit *yLine;
    QLineEdit *zLine;
    QLabel *label_4;
    QLineEdit *colorLine;
    QSpacerItem *verticalSpacer;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *pushButton_2;

    void setupUi(QDialog *TransformDialog)
    {
        if (TransformDialog->objectName().isEmpty())
            TransformDialog->setObjectName(QStringLiteral("TransformDialog"));
        TransformDialog->resize(411, 169);
        verticalLayout = new QVBoxLayout(TransformDialog);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        formLayout = new QFormLayout();
        formLayout->setSpacing(6);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        label = new QLabel(TransformDialog);
        label->setObjectName(QStringLiteral("label"));

        formLayout->setWidget(0, QFormLayout::LabelRole, label);

        label_2 = new QLabel(TransformDialog);
        label_2->setObjectName(QStringLiteral("label_2"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label_2);

        label_3 = new QLabel(TransformDialog);
        label_3->setObjectName(QStringLiteral("label_3"));

        formLayout->setWidget(2, QFormLayout::LabelRole, label_3);

        xLine = new QLineEdit(TransformDialog);
        xLine->setObjectName(QStringLiteral("xLine"));

        formLayout->setWidget(0, QFormLayout::FieldRole, xLine);

        yLine = new QLineEdit(TransformDialog);
        yLine->setObjectName(QStringLiteral("yLine"));

        formLayout->setWidget(1, QFormLayout::FieldRole, yLine);

        zLine = new QLineEdit(TransformDialog);
        zLine->setObjectName(QStringLiteral("zLine"));

        formLayout->setWidget(2, QFormLayout::FieldRole, zLine);

        label_4 = new QLabel(TransformDialog);
        label_4->setObjectName(QStringLiteral("label_4"));

        formLayout->setWidget(3, QFormLayout::LabelRole, label_4);

        colorLine = new QLineEdit(TransformDialog);
        colorLine->setObjectName(QStringLiteral("colorLine"));

        formLayout->setWidget(3, QFormLayout::FieldRole, colorLine);


        verticalLayout->addLayout(formLayout);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        pushButton_2 = new QPushButton(TransformDialog);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));

        horizontalLayout->addWidget(pushButton_2);


        verticalLayout->addLayout(horizontalLayout);


        retranslateUi(TransformDialog);
        QObject::connect(pushButton_2, SIGNAL(clicked()), TransformDialog, SLOT(ApplyButtonClicked()));

        QMetaObject::connectSlotsByName(TransformDialog);
    } // setupUi

    void retranslateUi(QDialog *TransformDialog)
    {
        TransformDialog->setWindowTitle(QApplication::translate("TransformDialog", "TransformDialog", 0));
        label->setText(QApplication::translate("TransformDialog", "X-coordinate:", 0));
        label_2->setText(QApplication::translate("TransformDialog", "Y-coordinate:", 0));
        label_3->setText(QApplication::translate("TransformDialog", "Z-coordinate:", 0));
        xLine->setText(QApplication::translate("TransformDialog", "x", 0));
        yLine->setText(QApplication::translate("TransformDialog", "y", 0));
        zLine->setText(QApplication::translate("TransformDialog", "z", 0));
        label_4->setText(QApplication::translate("TransformDialog", "Color function:", 0));
        colorLine->setText(QApplication::translate("TransformDialog", "0", 0));
        colorLine->setPlaceholderText(QString());
        pushButton_2->setText(QApplication::translate("TransformDialog", "Apply", 0));
    } // retranslateUi

};

namespace Ui {
    class TransformDialog: public Ui_TransformDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TRANSFORMDIALOG_H
