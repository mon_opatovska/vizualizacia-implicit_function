/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "QVTKWidget.h"

QT_BEGIN_NAMESPACE

class Ui_PlochoUI
{
public:
    QAction *actionExit;
    QAction *actionReset_Viewport;
    QAction *actionTorus;
    QAction *actionLinear;
    QAction *actionLoop;
    QAction *actionButterfly;
    QAction *actionDecimatePro;
    QAction *actionQuadratic_decimation;
    QAction *actionQuadratic_clustering;
    QAction *actionLaplacian_smooth;
    QAction *actionWindowSinc_smooth;
    QAction *actionPlane;
    QAction *actionSphere;
    QAction *actionColor;
    QAction *actionImplicit_function;
    QWidget *centralwidget;
    QGridLayout *gridLayout;
    QVBoxLayout *verticalLayout;
    QVTKWidget *qvtkWidget;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QMenu *menuTools;
    QMenu *menuCreate_shape;
    QMenu *menuSubdivision;
    QMenu *menuDecimate;
    QMenu *menuSmooth_mesh;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *PlochoUI)
    {
        if (PlochoUI->objectName().isEmpty())
            PlochoUI->setObjectName(QStringLiteral("PlochoUI"));
        PlochoUI->resize(678, 443);
        PlochoUI->setMinimumSize(QSize(0, 0));
        actionExit = new QAction(PlochoUI);
        actionExit->setObjectName(QStringLiteral("actionExit"));
        actionReset_Viewport = new QAction(PlochoUI);
        actionReset_Viewport->setObjectName(QStringLiteral("actionReset_Viewport"));
        actionTorus = new QAction(PlochoUI);
        actionTorus->setObjectName(QStringLiteral("actionTorus"));
        actionLinear = new QAction(PlochoUI);
        actionLinear->setObjectName(QStringLiteral("actionLinear"));
        actionLoop = new QAction(PlochoUI);
        actionLoop->setObjectName(QStringLiteral("actionLoop"));
        actionButterfly = new QAction(PlochoUI);
        actionButterfly->setObjectName(QStringLiteral("actionButterfly"));
        actionDecimatePro = new QAction(PlochoUI);
        actionDecimatePro->setObjectName(QStringLiteral("actionDecimatePro"));
        actionQuadratic_decimation = new QAction(PlochoUI);
        actionQuadratic_decimation->setObjectName(QStringLiteral("actionQuadratic_decimation"));
        actionQuadratic_clustering = new QAction(PlochoUI);
        actionQuadratic_clustering->setObjectName(QStringLiteral("actionQuadratic_clustering"));
        actionLaplacian_smooth = new QAction(PlochoUI);
        actionLaplacian_smooth->setObjectName(QStringLiteral("actionLaplacian_smooth"));
        actionWindowSinc_smooth = new QAction(PlochoUI);
        actionWindowSinc_smooth->setObjectName(QStringLiteral("actionWindowSinc_smooth"));
        actionPlane = new QAction(PlochoUI);
        actionPlane->setObjectName(QStringLiteral("actionPlane"));
        actionSphere = new QAction(PlochoUI);
        actionSphere->setObjectName(QStringLiteral("actionSphere"));
        actionColor = new QAction(PlochoUI);
        actionColor->setObjectName(QStringLiteral("actionColor"));
        actionImplicit_function = new QAction(PlochoUI);
        actionImplicit_function->setObjectName(QStringLiteral("actionImplicit_function"));
        centralwidget = new QWidget(PlochoUI);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        centralwidget->setMinimumSize(QSize(678, 374));
        gridLayout = new QGridLayout(centralwidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(-1, -1, 0, -1);
        qvtkWidget = new QVTKWidget(centralwidget);
        qvtkWidget->setObjectName(QStringLiteral("qvtkWidget"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(qvtkWidget->sizePolicy().hasHeightForWidth());
        qvtkWidget->setSizePolicy(sizePolicy);
        qvtkWidget->setMinimumSize(QSize(0, 0));

        verticalLayout->addWidget(qvtkWidget);


        gridLayout->addLayout(verticalLayout, 2, 0, 1, 1);

        PlochoUI->setCentralWidget(centralwidget);
        menuBar = new QMenuBar(PlochoUI);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 678, 21));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QStringLiteral("menuFile"));
        menuTools = new QMenu(menuBar);
        menuTools->setObjectName(QStringLiteral("menuTools"));
        menuCreate_shape = new QMenu(menuTools);
        menuCreate_shape->setObjectName(QStringLiteral("menuCreate_shape"));
        menuSubdivision = new QMenu(menuTools);
        menuSubdivision->setObjectName(QStringLiteral("menuSubdivision"));
        menuDecimate = new QMenu(menuTools);
        menuDecimate->setObjectName(QStringLiteral("menuDecimate"));
        menuSmooth_mesh = new QMenu(menuTools);
        menuSmooth_mesh->setObjectName(QStringLiteral("menuSmooth_mesh"));
        PlochoUI->setMenuBar(menuBar);
        statusBar = new QStatusBar(PlochoUI);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        PlochoUI->setStatusBar(statusBar);

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuTools->menuAction());
        menuFile->addAction(actionExit);
        menuTools->addAction(actionReset_Viewport);
        menuTools->addAction(menuCreate_shape->menuAction());
        menuTools->addAction(menuSubdivision->menuAction());
        menuTools->addAction(menuDecimate->menuAction());
        menuTools->addAction(menuSmooth_mesh->menuAction());
        menuCreate_shape->addAction(actionPlane);
        menuCreate_shape->addAction(actionSphere);
        menuCreate_shape->addAction(actionTorus);
        menuCreate_shape->addAction(actionImplicit_function);
        menuSubdivision->addAction(actionLinear);
        menuSubdivision->addAction(actionLoop);
        menuSubdivision->addAction(actionButterfly);
        menuDecimate->addAction(actionDecimatePro);
        menuDecimate->addAction(actionQuadratic_decimation);
        menuDecimate->addAction(actionQuadratic_clustering);
        menuSmooth_mesh->addAction(actionLaplacian_smooth);
        menuSmooth_mesh->addAction(actionWindowSinc_smooth);

        retranslateUi(PlochoUI);
        QObject::connect(actionExit, SIGNAL(triggered()), PlochoUI, SLOT(exit_clicked()));
        QObject::connect(actionReset_Viewport, SIGNAL(triggered()), PlochoUI, SLOT(reset_viewport_clicked()));
        QObject::connect(actionTorus, SIGNAL(triggered()), PlochoUI, SLOT(create_torus()));
        QObject::connect(actionLinear, SIGNAL(triggered()), PlochoUI, SLOT(LinearDiv()));
        QObject::connect(actionButterfly, SIGNAL(triggered()), PlochoUI, SLOT(ButterDiv()));
        QObject::connect(actionLoop, SIGNAL(triggered()), PlochoUI, SLOT(LoopDiv()));
        QObject::connect(actionDecimatePro, SIGNAL(triggered()), PlochoUI, SLOT(DecPro()));
        QObject::connect(actionQuadratic_clustering, SIGNAL(triggered()), PlochoUI, SLOT(DecClus()));
        QObject::connect(actionQuadratic_decimation, SIGNAL(triggered()), PlochoUI, SLOT(DecQuad()));
        QObject::connect(actionLaplacian_smooth, SIGNAL(triggered()), PlochoUI, SLOT(SmoothLap()));
        QObject::connect(actionWindowSinc_smooth, SIGNAL(triggered()), PlochoUI, SLOT(SmoothWin()));
        QObject::connect(actionPlane, SIGNAL(triggered()), PlochoUI, SLOT(create_plane()));
        QObject::connect(actionSphere, SIGNAL(triggered()), PlochoUI, SLOT(create_sphere()));
        QObject::connect(actionImplicit_function, SIGNAL(triggered()), PlochoUI, SLOT(create_implicit()));

        QMetaObject::connectSlotsByName(PlochoUI);
    } // setupUi

    void retranslateUi(QMainWindow *PlochoUI)
    {
        PlochoUI->setWindowTitle(QApplication::translate("PlochoUI", "PCLViewer", 0));
        actionExit->setText(QApplication::translate("PlochoUI", "Exit", 0));
        actionReset_Viewport->setText(QApplication::translate("PlochoUI", "Reset Viewport", 0));
        actionTorus->setText(QApplication::translate("PlochoUI", "Torus", 0));
        actionLinear->setText(QApplication::translate("PlochoUI", "Linear", 0));
        actionLoop->setText(QApplication::translate("PlochoUI", "Loop", 0));
        actionButterfly->setText(QApplication::translate("PlochoUI", "Butterfly", 0));
        actionDecimatePro->setText(QApplication::translate("PlochoUI", "DecimatePro", 0));
        actionQuadratic_decimation->setText(QApplication::translate("PlochoUI", "Quadratic decimation", 0));
        actionQuadratic_clustering->setText(QApplication::translate("PlochoUI", "Quadratic clustering", 0));
        actionLaplacian_smooth->setText(QApplication::translate("PlochoUI", "Laplacian smooth", 0));
        actionWindowSinc_smooth->setText(QApplication::translate("PlochoUI", "WindowedSinc smooth", 0));
        actionPlane->setText(QApplication::translate("PlochoUI", "Plane", 0));
        actionSphere->setText(QApplication::translate("PlochoUI", "Sphere", 0));
        actionColor->setText(QApplication::translate("PlochoUI", "Color", 0));
        actionImplicit_function->setText(QApplication::translate("PlochoUI", "Implicit function", 0));
        menuFile->setTitle(QApplication::translate("PlochoUI", "File", 0));
        menuTools->setTitle(QApplication::translate("PlochoUI", "Tools", 0));
        menuCreate_shape->setTitle(QApplication::translate("PlochoUI", "Create shape", 0));
        menuSubdivision->setTitle(QApplication::translate("PlochoUI", "Subdivision", 0));
        menuDecimate->setTitle(QApplication::translate("PlochoUI", "Decimate", 0));
        menuSmooth_mesh->setTitle(QApplication::translate("PlochoUI", "Smooth mesh", 0));
    } // retranslateUi

};

namespace Ui {
    class PlochoUI: public Ui_PlochoUI {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
