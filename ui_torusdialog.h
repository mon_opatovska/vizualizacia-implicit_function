/********************************************************************************
** Form generated from reading UI file 'torusdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TORUSDIALOG_H
#define UI_TORUSDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_TorusDialog
{
public:
    QVBoxLayout *verticalLayout;
    QFormLayout *formLayout;
    QLabel *label;
    QDoubleSpinBox *ringRadiusSpin;
    QLabel *label_2;
    QDoubleSpinBox *crossSectSpin;
    QLabel *label_3;
    QSpinBox *uResSpin;
    QLabel *label_4;
    QSpinBox *vResSpin;
    QSpacerItem *verticalSpacer;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *pushButton;
    QPushButton *pushButton_2;

    void setupUi(QDialog *TorusDialog)
    {
        if (TorusDialog->objectName().isEmpty())
            TorusDialog->setObjectName(QStringLiteral("TorusDialog"));
        TorusDialog->resize(291, 180);
        verticalLayout = new QVBoxLayout(TorusDialog);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        formLayout = new QFormLayout();
        formLayout->setSpacing(6);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        label = new QLabel(TorusDialog);
        label->setObjectName(QStringLiteral("label"));

        formLayout->setWidget(0, QFormLayout::LabelRole, label);

        ringRadiusSpin = new QDoubleSpinBox(TorusDialog);
        ringRadiusSpin->setObjectName(QStringLiteral("ringRadiusSpin"));
        ringRadiusSpin->setValue(5);

        formLayout->setWidget(0, QFormLayout::FieldRole, ringRadiusSpin);

        label_2 = new QLabel(TorusDialog);
        label_2->setObjectName(QStringLiteral("label_2"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label_2);

        crossSectSpin = new QDoubleSpinBox(TorusDialog);
        crossSectSpin->setObjectName(QStringLiteral("crossSectSpin"));
        crossSectSpin->setValue(2);

        formLayout->setWidget(1, QFormLayout::FieldRole, crossSectSpin);

        label_3 = new QLabel(TorusDialog);
        label_3->setObjectName(QStringLiteral("label_3"));

        formLayout->setWidget(2, QFormLayout::LabelRole, label_3);

        uResSpin = new QSpinBox(TorusDialog);
        uResSpin->setObjectName(QStringLiteral("uResSpin"));
        uResSpin->setMinimum(10);
        uResSpin->setMaximum(3000);
        uResSpin->setValue(100);

        formLayout->setWidget(2, QFormLayout::FieldRole, uResSpin);

        label_4 = new QLabel(TorusDialog);
        label_4->setObjectName(QStringLiteral("label_4"));

        formLayout->setWidget(3, QFormLayout::LabelRole, label_4);

        vResSpin = new QSpinBox(TorusDialog);
        vResSpin->setObjectName(QStringLiteral("vResSpin"));
        vResSpin->setMinimum(10);
        vResSpin->setMaximum(3000);
        vResSpin->setValue(100);

        formLayout->setWidget(3, QFormLayout::FieldRole, vResSpin);


        verticalLayout->addLayout(formLayout);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(-1, 0, -1, -1);
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        pushButton = new QPushButton(TorusDialog);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        horizontalLayout->addWidget(pushButton);

        pushButton_2 = new QPushButton(TorusDialog);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));

        horizontalLayout->addWidget(pushButton_2);


        verticalLayout->addLayout(horizontalLayout);


        retranslateUi(TorusDialog);
        QObject::connect(pushButton, SIGNAL(clicked()), TorusDialog, SLOT(accept()));
        QObject::connect(pushButton_2, SIGNAL(clicked()), TorusDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(TorusDialog);
    } // setupUi

    void retranslateUi(QDialog *TorusDialog)
    {
        TorusDialog->setWindowTitle(QApplication::translate("TorusDialog", "TorusDialog", 0));
        label->setText(QApplication::translate("TorusDialog", "Ring radius", 0));
        label_2->setText(QApplication::translate("TorusDialog", "Cross section radius", 0));
        label_3->setText(QApplication::translate("TorusDialog", "U Resolution", 0));
        label_4->setText(QApplication::translate("TorusDialog", "V Resolution", 0));
        pushButton->setText(QApplication::translate("TorusDialog", "OK", 0));
        pushButton_2->setText(QApplication::translate("TorusDialog", "Cancel", 0));
    } // retranslateUi

};

namespace Ui {
    class TorusDialog: public Ui_TorusDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TORUSDIALOG_H
