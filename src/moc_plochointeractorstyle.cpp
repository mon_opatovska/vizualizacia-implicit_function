/****************************************************************************
** Meta object code from reading C++ file 'plochointeractorstyle.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.7.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../src/plochointeractorstyle.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'plochointeractorstyle.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.7.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_PlochoInteractorStyle_t {
    QByteArrayData data[5];
    char stringdata0[69];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_PlochoInteractorStyle_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_PlochoInteractorStyle_t qt_meta_stringdata_PlochoInteractorStyle = {
    {
QT_MOC_LITERAL(0, 0, 21), // "PlochoInteractorStyle"
QT_MOC_LITERAL(1, 22, 12), // "UpdateWidget"
QT_MOC_LITERAL(2, 35, 0), // ""
QT_MOC_LITERAL(3, 36, 11), // "ClearPoints"
QT_MOC_LITERAL(4, 48, 20) // "UpdatePointPositions"

    },
    "PlochoInteractorStyle\0UpdateWidget\0\0"
    "ClearPoints\0UpdatePointPositions"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PlochoInteractorStyle[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   29,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       3,    0,   30,    2, 0x0a /* Public */,
       4,    0,   31,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void PlochoInteractorStyle::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        PlochoInteractorStyle *_t = static_cast<PlochoInteractorStyle *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->UpdateWidget(); break;
        case 1: _t->ClearPoints(); break;
        case 2: _t->UpdatePointPositions(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (PlochoInteractorStyle::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&PlochoInteractorStyle::UpdateWidget)) {
                *result = 0;
                return;
            }
        }
    }
    Q_UNUSED(_a);
}

const QMetaObject PlochoInteractorStyle::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_PlochoInteractorStyle.data,
      qt_meta_data_PlochoInteractorStyle,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *PlochoInteractorStyle::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PlochoInteractorStyle::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_PlochoInteractorStyle.stringdata0))
        return static_cast<void*>(const_cast< PlochoInteractorStyle*>(this));
    if (!strcmp(_clname, "vtkInteractorStyleRubberBandPick"))
        return static_cast< vtkInteractorStyleRubberBandPick*>(const_cast< PlochoInteractorStyle*>(this));
    return QObject::qt_metacast(_clname);
}

int PlochoInteractorStyle::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 3)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 3;
    }
    return _id;
}

// SIGNAL 0
void PlochoInteractorStyle::UpdateWidget()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
