/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.7.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../src/mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.7.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_PlochoUI_t {
    QByteArrayData data[19];
    char stringdata0[211];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_PlochoUI_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_PlochoUI_t qt_meta_stringdata_PlochoUI = {
    {
QT_MOC_LITERAL(0, 0, 8), // "PlochoUI"
QT_MOC_LITERAL(1, 9, 22), // "reset_viewport_clicked"
QT_MOC_LITERAL(2, 32, 0), // ""
QT_MOC_LITERAL(3, 33, 12), // "exit_clicked"
QT_MOC_LITERAL(4, 46, 12), // "UpdateWidget"
QT_MOC_LITERAL(5, 59, 12), // "create_torus"
QT_MOC_LITERAL(6, 72, 12), // "create_plane"
QT_MOC_LITERAL(7, 85, 13), // "create_sphere"
QT_MOC_LITERAL(8, 99, 15), // "create_implicit"
QT_MOC_LITERAL(9, 115, 9), // "transform"
QT_MOC_LITERAL(10, 125, 14), // "ApplyTransform"
QT_MOC_LITERAL(11, 140, 9), // "LinearDiv"
QT_MOC_LITERAL(12, 150, 7), // "LoopDiv"
QT_MOC_LITERAL(13, 158, 9), // "ButterDiv"
QT_MOC_LITERAL(14, 168, 6), // "DecPro"
QT_MOC_LITERAL(15, 175, 7), // "DecQuad"
QT_MOC_LITERAL(16, 183, 7), // "DecClus"
QT_MOC_LITERAL(17, 191, 9), // "SmoothLap"
QT_MOC_LITERAL(18, 201, 9) // "SmoothWin"

    },
    "PlochoUI\0reset_viewport_clicked\0\0"
    "exit_clicked\0UpdateWidget\0create_torus\0"
    "create_plane\0create_sphere\0create_implicit\0"
    "transform\0ApplyTransform\0LinearDiv\0"
    "LoopDiv\0ButterDiv\0DecPro\0DecQuad\0"
    "DecClus\0SmoothLap\0SmoothWin"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PlochoUI[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      17,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   99,    2, 0x0a /* Public */,
       3,    0,  100,    2, 0x0a /* Public */,
       4,    0,  101,    2, 0x0a /* Public */,
       5,    0,  102,    2, 0x0a /* Public */,
       6,    0,  103,    2, 0x0a /* Public */,
       7,    0,  104,    2, 0x0a /* Public */,
       8,    0,  105,    2, 0x0a /* Public */,
       9,    0,  106,    2, 0x0a /* Public */,
      10,    0,  107,    2, 0x0a /* Public */,
      11,    0,  108,    2, 0x0a /* Public */,
      12,    0,  109,    2, 0x0a /* Public */,
      13,    0,  110,    2, 0x0a /* Public */,
      14,    0,  111,    2, 0x0a /* Public */,
      15,    0,  112,    2, 0x0a /* Public */,
      16,    0,  113,    2, 0x0a /* Public */,
      17,    0,  114,    2, 0x0a /* Public */,
      18,    0,  115,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void PlochoUI::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        PlochoUI *_t = static_cast<PlochoUI *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->reset_viewport_clicked(); break;
        case 1: _t->exit_clicked(); break;
        case 2: _t->UpdateWidget(); break;
        case 3: _t->create_torus(); break;
        case 4: _t->create_plane(); break;
        case 5: _t->create_sphere(); break;
        case 6: _t->create_implicit(); break;
        case 7: _t->transform(); break;
        case 8: _t->ApplyTransform(); break;
        case 9: _t->LinearDiv(); break;
        case 10: _t->LoopDiv(); break;
        case 11: _t->ButterDiv(); break;
        case 12: _t->DecPro(); break;
        case 13: _t->DecQuad(); break;
        case 14: _t->DecClus(); break;
        case 15: _t->SmoothLap(); break;
        case 16: _t->SmoothWin(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObject PlochoUI::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_PlochoUI.data,
      qt_meta_data_PlochoUI,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *PlochoUI::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PlochoUI::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_PlochoUI.stringdata0))
        return static_cast<void*>(const_cast< PlochoUI*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int PlochoUI::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 17)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 17;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 17)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 17;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
